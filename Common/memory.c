/*
** memory.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Apr  8 12:32:47 2014 Mano S. NGUYEN
** Last update Thu Apr 10 19:17:26 2014 Mano S. NGUYEN
*/

#include <stddef.h>
#include "memory.h"

/*
** void	my_memset(void *ptr, int value, size_t size)
**
** Fill size bytes  with value from *ptr
**
*/
void		my_memset(void *ptr, int value, size_t size)
{
  size_t	i;
  char		*mem;

  i = 0;
  mem = (char *)ptr;
  while (i < size)
    {
      mem[i] = (unsigned char)value;
      ++i;
    }
}

/*
** void	my_memcpy(void *desc, void *src, size_t size)
**
** Copy a chunk of data from *src to *desc
**
*/
void		my_memcpy(void *desc, void *src, size_t size)
{
  size_t	i;
  char		*cdesc;
  char		*csrc;

  i = 0;
  cdesc = (char *)desc;
  csrc = (char *)src;
  while (i < size)
    {
      cdesc[i] = csrc[i];
      ++i;
    }
}

/*
** int	my_memrev(int nb)
**
** Return a BigEndian to LittleEndian (reversed) number
**
*/
int		my_memrev(const int nb)
{
  int		out;
  char		swap;
  unsigned char	*ct;

  out = nb;
  ct = (unsigned char *)(&out);
  swap = ct[0];
  ct[0] = ct[3];
  ct[3] = swap;
  swap = ct[1];
  ct[1] = ct[2];
  ct[2] = swap;
  return (out);
}

/*
** void	my_memclear(void *ptr, size_t size)
**
** Erase size bytes in memory from *ptr
**
*/
void	my_memclear(void *ptr, size_t size)
{
  my_memset(ptr, 0, size);
}

/*
** int	my_getendian()
**
** Return a number according to the host computer endianness
**	MEM_BIG_ENDIAN
**	MEM_LITTLE_ENDIAN
**	MEM_UNKNOWN_ENDIANNESS
**
*/
int	my_getendian()
{
  unsigned int		i;
  unsigned char*	ct;

  i = 0x12345678;
  ct = (unsigned char *)(&i);
  if (ct[0] == 0x12)
    return (MEM_BIG_ENDIAN);
  else if (ct[0] == 0x78)
    return (MEM_LITTLE_ENDIAN);
  return (MEM_UNKNOWN_ENDIANNESS);
}
