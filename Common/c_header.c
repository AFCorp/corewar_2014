/*
** c_header.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Feb 25 19:31:04 2014 Mano S. NGUYEN
** Last update Wed Apr  9 14:55:30 2014 Mano S. NGUYEN
*/

#include <unistd.h>
#include <stdlib.h>
#include "memory.h"
#include "strfuncs.h"
#include "io.h"
#include "op.h"

/*
** header_t c_header_create(char *, char *, int)
**
** Create a .cor header struct
*/
header_t	*c_header_create(char *name, char *comment, int size)
{
  header_t	*out;

  if (my_strlen(name) > PROG_NAME_LENGTH || my_strlen(comment) > COMMENT_LENGTH)
    return (NULL);
  if ((out = malloc(sizeof(header_t))) == NULL)
    return (NULL);
  out->magic = my_memrev(COREWAR_EXEC_MAGIC);
  my_strcpy(out->prog_name, name);
  out->prog_size = my_memrev(size);
  my_strcpy(out->comment, comment);
  return (out);
}

/*
** int c_header_write(const int, header_t *)
**
** Write a .cor header struct to a previously opened file descriptor
*/
int	c_header_write(const int fd, header_t *in)
{
  if (in == NULL)
    return (-2);
  return (write(fd, in, sizeof(header_t)));
}


/*
** header_t c_header_read(const int)
**
** Read a .cor header struct from a previously opened file descriptor
*/
header_t	*c_header_read(const int fd)
{
  header_t	*out;

  if ((out = malloc(sizeof(header_t))) == NULL)
    return (NULL);
  if (read(fd, out, sizeof(header_t)) <= 0)
    {
      free(out);
      return (NULL);
    }
  return (out);
}
