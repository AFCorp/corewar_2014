/*
** strfuncs.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Feb 25 19:38:09 2014 Mano S. NGUYEN
** Last update Tue Feb 25 20:11:50 2014 Mano S. NGUYEN
*/

#include <stdlib.h>

int	my_strlen(const char *str)
{
  int	size;

  if (str == NULL)
    return (0);
  size = 0;
  while (str[size] != '\0')
    ++size;
  return (size);
}

char	*my_strcpy(char *dest, const char *src)
{
  int	i;

  if (dest == NULL || src == NULL)
    return (NULL);
  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = '\0';
  return (dest);
}
