/*
** printf.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common/io
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Fri Apr 11 16:53:12 2014 Mano S. NGUYEN
** Last update Sat Apr 12 22:06:10 2014 Guillaume
*/

#include <stdarg.h>
#include "io.h"
#include "strfuncs.h"

int		my_printf(const char *format, ...)
{
  int		idx;
  va_list	ap;

  va_start(ap, format);
  idx = 0;
  while (idx < my_strlen(format))
    {
      if (check_char(format, &idx) == 0)
	{
	  if (format[idx] == 'd')
	    my_putnbr_base(va_arg(ap, int), "0123456789");
	  else if (format[idx] == 'h')
	    my_putnbr_base(va_arg(ap, int), "0123456789abcdef");
	  else if (format[idx] == 'p')
	    {
	      my_putstr("0x");
	      my_putnbr_base((int)((long)va_arg(ap, void *)), "0123456789ABCDEF");
	    }
	  else if (format[idx] == 's')
	    my_putstr(va_arg(ap, char *));
	  ++idx;
	}
    }
  va_end(ap);
  return (my_strlen(format));
}

/*
** Check if char IS '%' (and if next char IS NOT '%')
** write this char to STDOUT in other case.
**
** Return: 0 for printable char, 1 if need va_arg call
*/
int	check_char(const char* str, int *index)
{
  if (str[(*index)] != '%')
    {
      my_putchar(str[(*index)]);
      ++(*index);
      return (1);
    }
  else
    {
      if (str[++(*index)] == '%')
	{
	  my_putchar('%');
	  ++(*index);
	  return (1);
	}
      return (0);
    }
}
