/*
** iostr.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common/io
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Fri Apr  4 17:01:35 2014 Mano S. NGUYEN
** Last update Fri Apr  4 17:03:55 2014 Mano S. NGUYEN
*/
#include <unistd.h>
#include "strfuncs.h"
#include "io.h"

int	my_putchar(const char c)
{
  return (write(STDOUT, &c, 1));
}

int	my_fputstr(const int fd, const char *str)
{
  return (write(fd, str, my_strlen(str)));
}

int	my_putstr(const char *str)
{
  return (my_fputstr(STDOUT, str));
}

int	my_eputstr(const char *str)
{
  return (my_fputstr(STDERR, str));
}

int	my_showstr(const char *str)
{
  int	index;
  int	count;

  index = 0;
  count = 0;
  while (str[index] != '\0')
    {
      if (str[index] >= 32 && str[index] < 127)
	my_putchar(str[index]);
      else
	{
	  count += my_putstr("\\0");
	  count += my_putnbr_base(str[index], "01234567");
	}
      ++index;
      ++count;
    }
  return (count);
}
