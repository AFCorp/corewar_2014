/*
** ionbr.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common/io
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Fri Apr  4 17:10:55 2014 Mano S. NGUYEN
** Last update Thu Apr 10 19:17:54 2014 Mano S. NGUYEN
*/

#include "strfuncs.h"
#include "io.h"

int	power_rec(const int nb, const int pow)
{
  if (pow == 0)
    return (1);
  if (pow == 1)
    return (nb);
  else
    return (nb * power_rec(nb, pow - 1));
}

int	is_prstr(const char *str, const char c)
{
  int	count;

  count = 0;
  while (str[count] != '\0')
    {
      if (str[count] == c)
	return (count);
      ++count;
    }
  return (-1);
}

int	my_getnbr_base(const char *str, const char *base)
{
  int	index;
  int	start;
  int	size;
  int	minus;
  int	number;

  index = 0;
  minus = 0;
  while (str[index] != '\0' && is_prstr(base, str[index]) < 0)
    {
      if (str[index++] == '-')
	++minus;
    }
  start = index;
  size = 0;
  while (str[index++] != '\0' && is_prstr(base, str[index - 1]) >= 0)
    ++size;
  number = 0;
  index = start + 1;
  while ((index - start) <= size)
    {
      number += power_rec(my_strlen(base), size + start - index)
	* is_prstr(base, str[index - 1]);
      ++index;
    }
  return ((minus % 2) ? -number : number);
}

int	my_putnbr_base(int n, const char *base)
{
  int	pow;
  int	ccount;

  if (n == 0)
    my_putchar(base[0]);
  ccount = 0;
  if (n < 0)
    {
      n = -n;
      my_putchar('-');
      ++ccount;
    }
  pow = 1;
  while (pow < n)
    pow = pow * my_strlen(base);
  if (((n > my_strlen(base)) && (n % pow)) || (n < my_strlen(base) && n != 1))
    pow = pow / my_strlen(base);
  while (pow > 0)
    {
      my_putchar(base[n / pow]);
      n = n % pow;
      pow = pow / my_strlen(base);
      ++ccount;
    }
  return (ccount);
}
