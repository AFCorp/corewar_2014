# Corewar 2014

Bienvenue sur le repository ***Assembly & Fighting Corp.*** !

Equipe:

* **nguye_g**
* **nato_i**
* **potier_g**
* **tahar_w**

## Documents
Description du projet, Timelines Gantt, Idees diverses, etc... tout est la !

* [Dossier](https://drive.google.com/folderview?id=0B9mlcC3m1XMlVkNhMGNlc0o4Qnc) mis a disposition par tahar_w

## TODO LIST
Progression du projet: 

**COREWAR**:

* Parsing des *.cor*
* Chargement des *.cor* (structure)
* Allocation de la *pool*
* Chargement des *.cor* dans la *pool*
* Chargement de l'ordennanceur
* **[Loop]** Implementation des *cycles*
* **[Loop]** Execution des *stacks* / *PC*
* **[Loop]** Update des structures champions
* **[Loop]** *Who's still alive ?*
* Nettoyage

**ASM**:

* Parsing des *.s*
* Gestion d'erreur
* Analyse du code
* Generation des *opcodes*

## Contribution
Avant toute chose, assurez-vous de bien avoir uploader votre **Cle SSH** !

* A la 1ere utilisation **SEULEMENT**:
Cloner le repo: `git clone https://your_username@bitbucket.org/AFCorp/corewar_2014.git`
* Avant chaque modification **ET** chaque *commit* (pour eviter les conflits)
`git pull origin master`
* Modififiez vos sources !
* Ajoutez vos fichier au *commit*:
`git add fichier...`
* En cas de suppression de fichier, utilisez `git rm fichier...` et pas le `rm` du shell !
* Commitez vos changements:
`git commit -m "Description du commit"`
* Envoyer votre *commit* sur le serveur:
`git push origin master`

**EN CAS D'ERREUR:**

Si vous voulez annuler, une ou plusieurs modifications, **AVANT** un *push*

* Recuperer un fichier depuis le serveur:
`git checkout -- fichier...`
* Annuler tout vos changement et recuperer le dernier *commit* du serveur: 
`git fetch origin` puis `git reset --hard origin/master`

Enfin, en cas de **conflit**, meme si on espere que ca n'arrivera jamiais...
**REUNION D'URGENCE** !

## Utilisation
* Cloner le repository
`git clone https://your_username@bitbucket.org/AFCorp/corewar_2014.git`
* Compiler la VM et/ou l'ASM `make corewar asm`
* Faites vous plaisir !

## ALORS, AU BOULOT !
-![alt text](https://dl.dropboxusercontent.com/u/198192493/keyboardcat.gif "Le chat qui code...")
[t](http://www.google.fr)
