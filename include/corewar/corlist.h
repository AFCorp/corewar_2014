/*
** corlist.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Apr  8 17:25:25 2014 Mano S. NGUYEN
** Last update Sat Apr 12 20:49:47 2014 Mano S. NGUYEN
*/

#ifndef CORLIST_COR_H_
# define CORLIST_COR_H_

# include "op.h"

/*
** COR Management chained list struct
*/

typedef struct	s_cor
{
  char		name[PROG_NAME_LENGTH + 1];
  char		comment[COMMENT_LENGTH + 1];
  int		size;
  int		id_cor;
  int		live;
  int		count_before_death;
  int		cycle_to_exec;
  char		reg[REG_NUMBER][REG_SIZE];
  char		carry;
  char		*pc;
  struct s_cor	*next;
}		t_cor;

/*
** FUNCTION PROTOTYPES
*/
int	op_mgr(char *pc);
t_cor	*corlist_new(int);
int	corlist_load(t_cor *, int);
t_cor	*corlist_add(t_cor **, t_cor *);

void	cor_loop(t_cor *);

#endif	/* !CORLIST_COR_H_ */
