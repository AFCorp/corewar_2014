/*
** op_mgr.h for  in /home/nato_i/corewar_2014/include/corewar
** 
** Made by nato_i
** Login   <nato_i@epitech.net>
** 
** Started on  Sat Apr 12 19:29:57 2014 nato_i
** Last update Sat Apr 12 19:50:49 2014 nato_i
*/

#ifndef OP_MGR
# define OP_MGR

#define PARAM_NULL	0
#define PARAM_REG	1
#define PARAM_DIR	2
#define PARAM_IND	3


#endif /* !OP_MGR */
