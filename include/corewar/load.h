/*
** load.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Wed Apr  9 17:18:13 2014 Mano S. NGUYEN
** Last update Thu Apr 10 19:16:36 2014 Mano S. NGUYEN
*/

#ifndef	LOAD_COR_H_
# define LOAD_COR_H_

# include "corewar/corlist.h"

/*
**	FUNCTION PROTOTYPES
*/
int	pool_init();
int	pool_fill(int [], t_cor *);
int	*open_cor(int, int, char *[]);
void	load_code(int, char *, t_cor *);
t_cor	*load_cor(int []);

#endif /* !LOAD_COR_H_ */
