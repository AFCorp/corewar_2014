/*
** io.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/include
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Fri Apr  4 17:15:00 2014 Mano S. NGUYEN
** Last update Fri Apr 11 17:25:51 2014 Mano S. NGUYEN
*/

#ifndef IO_COR_H_
# define IO_COR_H_

/*
**	STANDARD FILE DESCRITORS
*/
# define STDIN	0
# define STDOUT	1
# define STDERR	2

/*
**	NBR FUNCTION MACRO
*/
# define my_get_nbr(ptr) my_getnbr_base(ptr, "0123456789")

/*
**	STR FUNCTION PROTOTYPES
*/
int	my_putchar(const char);
int	my_fputstr(const int, const char *);
int	my_putstr(const char *);
int	my_eputstr(const char *);
int	my_showstr(const char *);
int	my_strlen(const char *);

/*
**	NBR FUNCTION PROTOTYPES
*/
int	power_rec(const int, const int);
int	is_prstr(const char *, const char);
int	my_getnbr_base(const char *, const char *);
int	my_putnbr_base(int, const char *);

/*
**	PRINTF FUNCTION PROTOTYPES
*/
int	my_printf(const char *, ...);
int	check_char(const char *, int *);

#endif /* !IO_COR_H_ */
