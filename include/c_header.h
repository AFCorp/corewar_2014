/*
** c_header.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Feb 25 20:29:08 2014 Mano S. NGUYEN
** Last update Tue Apr  8 17:54:30 2014 Mano S. NGUYEN
*/

#ifndef COR_CHEADER_H_
# define COR_CHEADER_H_

/*
** Common/c_header.c
*/
header_t	*c_header_create(char *, char *, int);
int		c_header_write(const int, header_t *);
header_t	*c_header_read(const int);

#endif /* !COR_CHEADER_H_ */
