/*
** strfunc.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Feb 25 20:12:13 2014 Mano S. NGUYEN
** Last update Tue Feb 25 20:14:21 2014 Mano S. NGUYEN
*/

#ifndef COR_STRFUNC_H_
# define COR_STRFUNC_H_

/*
** Common/strfunc.c
*/
int	my_strlen(const char *);
char	*my_strcpy(char *, const char *);

#endif /* !COR_STRFUNC_H_ */
