/*
** asm.h for asm in /home/potier_g/corewar_2014/include/asm
** 
** Made by Guillaume
** potier_g   <potier_g@epitech.net>
** 
** Started on  Tue Apr  8 21:08:55 2014 Guillaume
** Last update Sat Apr 12 22:04:43 2014 Guillaume
*/

#ifndef ASM_H
# define ASM_H

#include "op.h"

typedef struct	s_asm
{
  char		*name_of_file;
  char		*file;
  char		*cor;
  char		*name;
  char		*comment;
  char		*oct0;
  int		size;
  int		s_fd;
  int		cor_fd;
  int		pos_line;
  int		pos_word;
}		t_asm;

int		registre(char *nb, int sign, char *line);
int		direct(char *nb, int sign, char *line);
int		indirect(char *nb, int sign, char *line);
char		*my_recup_line(char *file, int line);
int		char_select(char c);
int		detect_pos(char *line, int nb_word, int pos);
int		detect_len(char *line, int pos);
char		*my_detect_str(char *line, int nb_word);
int		oct_null(int oct, int nb_oct, int fd);
int		detect_fonct_oct(char *oct);
int		detect_oct2(char *oct, char *oct0);
int		nb_oct_to_line(char *line);
int		init_nb_oct(t_asm *p, int line);
int		detect_size(int line, t_asm *p);
int		my_size_prog(char *file);
char		*my_read(char *filename, t_asm *p);
int		my_match(char *str1, char *str2);
int		my_pow_rec(int nb, int power);
char		*my_strdup(char *str);
char		*my_alloc(int size);
char		*my_strcat(char *str1, char *str2);
int		my_power_rec(int nb, int power);
char		*my_convert_dec(int nbr, int len, char *base);
int		my_convert_ascii(int nbr, int fd);
int		my_recup_int_char(char *base, char c);
int		my_convert_in_to_dec(char *nbr, char *base);
int		my_check_name(t_asm *p);
int		my_check_comment(t_asm *p);
int		my_check_size(t_asm *p);
int		my_check_header(t_asm *p);
int		check_error(char *word, int line);
int		analyse_fonct(char *line, int sign, int l, t_asm *p);
int		my_analyse(char *line, int l, t_asm *p);
int		check_extention(char *name);
int		my_init_asm(int ac, char **av, t_asm *p);
int		my_asm(t_asm *p);
char		*cor(char *name);
char		*detect_oct(char *oct);
int		reconize_oct(char *oct1, char *oct2, char *oct3, t_asm *p);
header_t	*c_header_create(char *name, char *comment, int size);
int		c_header_write(const int fd, header_t *in);
header_t	*c_header_read(const int fd);
int		detect_fonct(char *fonct, t_asm *p);
int		detect_int(char *nb);
int		code_nbr_nb_oct(char *nb, int nb_oct, t_asm *p, int nb_l);
int		code_oct(char *nb, t_asm *p, int nb_oct, int nb_l);
int		asign_oct(char *oct, t_asm *p, int nb_l, int nb_o);

#endif /* !ASM_H */
