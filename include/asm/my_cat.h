/*
** get_next_line.h for get_next_line.h in /home/tahar_w/rendu/CPE_2013_getnextline
** 
** Made by tahar_w
** Login   <tahar_w@epitech.net>
** 
** Started on  Fri Nov 22 11:31:30 2013 tahar_w
** Last update Tue Mar 18 16:37:19 2014 tahar_w
*/

#include <unistd.h>

#ifndef MY_CAT_H_
# define  MY_CAT_H_

# define BUFFER_SIZE	4096
# define READ(buff, fd) (read(fd, buff, BUFFER_SIZE))

#endif /* !MY_CAT_H_ */
