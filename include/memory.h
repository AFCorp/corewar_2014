/*
** memory.h for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Common
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Apr  8 12:40:20 2014 Mano S. NGUYEN
** Last update Thu Apr 10 18:51:04 2014 Mano S. NGUYEN
*/

#ifndef	MEM_COR_H_
# define MEM_COR_H_

# include <stddef.h>

/*
**	MEMORY CONSTANTS
*/
# define MEM_BIG_ENDIAN		0
# define MEM_LITTLE_ENDIAN	1
# define MEM_UNKNOWN_ENDIANNESS	-1
/*
**	FUNCTION PROTOTYPES
*/
void	my_memset(void *, int, size_t);
void	my_memcpy(void *, void *, size_t);
void	my_memclear(void *, size_t);
int	my_memrev(const int);
int	my_getendian();

#endif	/* !MEM_COR_H_ */

