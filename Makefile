##
## Makefile for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014
## 
## Made by Mano S. NGUYEN
## Login   <nguye_g@epitech.net>
## 
## Started on  Thu Feb 20 11:36:20 2014 Mano S. NGUYEN
## Last update Sun Apr 13 18:38:24 2014 nato_i

CC		= gcc
RM		= @rm -f -v
ECHO		= @echo -e "\e[0;31;1m"
WCOLOR		= @echo -ne "\e[0;36;1m"
NCOLOR		= @echo -ne "\e[0m"

CFLAGS		+= -ansi -pedantic
CFLAGS		+= -Wall -Wextra
CFLAGS		+= -I./include/ -g3

NAME_COR	= corewar
NAME_ASM	= asm

SRC_COR		= ./Corewar/main_cor.c		\
	  	  ./Corewar/corlist.c		\
	  	  ./Corewar/op_mgr.c		\
	  	  ./Corewar/load.c		\
	  	  ./Corewar/loop.c		\
	  	  ./Common/c_header.c		\
	  	  ./Common/strfuncs.c		\
	  	  ./Common/memory.c		\
	  	  ./Common/io/iostr.c		\
	  	  ./Common/io/ionbr.c		\
	  	  ./Common/io/my_printf.c

SRC_ASM		= ./Asm/main.c			\
		  ./Asm/cor.c			\
		  ./Asm/my_fonct01.c		\
		  ./Asm/my_size_prog.c		\
		  ./Asm/my_check_header.c	\
		  ./Asm/my_match.c		\
		  ./Asm/my_read.c		\
		  ./Asm/my_analyse.c		\
		  ./Asm/asign_oct.c		\
		  ./Asm/nb_oct.c		\
		  ./Asm/code.c			\
		  ./Asm/oct_null.c		\
		  ./Asm/my_convert_dec.c	\
		  ./Asm/recup_line.c		\
		  ./Common/op.c			\
		  ./Common/c_header.c		\
		  ./Common/strfuncs.c		\
		  ./Common/io/iostr.c		\
		  ./Common/io/ionbr.c 		\
		  ./Common/io/my_printf.c 	\
		  ./Common/memory.c


OBJ_COR		= $(SRC_COR:.c=.o)
OBJ_ASM		= $(SRC_ASM:.c=.o)

all: $(NAME_COR) $(NAME_ASM)

$(NAME_COR):	$(OBJ_COR)
	$(ECHO) Linking $(NAME_COR)...
	$(NCOLOR)
	$(CC) $(OBJ_COR) -o $(NAME_COR)

$(NAME_ASM):	$(OBJ_ASM)
	$(ECHO) Linking $(NAME_ASM)...
	$(NCOLOR)
	$(CC) $(OBJ_ASM) -o $(NAME_ASM)

clean:
	$(ECHO) Cleaning $(NAME_COR) objects...
	$(WCOLOR)
	$(RM) $(OBJ_COR)
	$(ECHO) Cleaning $(NAME_ASM) objects...
	$(WCOLOR)
	$(RM) $(OBJ_ASM)

fclean:	clean
	$(ECHO) Cleaning $(NAME_COR) binary...
	$(WCOLOR)
	$(RM) $(NAME_COR)
	$(ECHO) Cleaning $(NAME_ASM) binary...
	$(WCOLOR)
	$(RM) $(NAME_ASM)

re:	fclean all

.PHONY: all clean fclean re
