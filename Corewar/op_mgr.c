/*
** op_mgr.c for  in /home/nato_i/corewar_2014/Corewar
** 
** Made by nato_i
** Login   <nato_i@epitech.net>
** 
** Started on  Sat Apr 12 19:20:36 2014 nato_i
** Last update Sun Apr 13 17:37:43 2014 nato_i
*/

#include "op.h"
#include "corewar/op_mgr.h"

int	op_mgr(char *pc)
{
  int	size;
  int	i;

  if (pc[0] == 0x01)
    return (5);
  else if (pc[0] == 0x09 || pc[0] == 0x0c || pc[0] == 0x0f)
    return (1 + IND_SIZE);
  size = 2;
  i = 0;
  while (i < MAX_ARGS_NUMBER)
    {
      if (pc[1] & (PARAM_NULL << (i * 2)))
	size += 0;
      else if (pc[1] & (PARAM_REG << (i * 2)))
	size += REG_SIZE;
      else if (pc[1] & (PARAM_DIR << (i * 2)))
	size += DIR_SIZE;
      else if (pc[1] & (PARAM_IND << (i * 2)))
	size += IND_SIZE;
     ++i ;
    }
  return (size);
}
