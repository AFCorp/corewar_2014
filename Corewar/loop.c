/*
** loop.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Apr  8 12:18:18 2014 Mano S. NGUYEN
<<<<<<< HEAD
** Last update Sun Apr 13 18:46:57 2014 nato_i
=======
** Last update Sun Apr 13 15:51:36 2014 nato_i
>>>>>>> 3c2e6862bb03d0b2ee4d59a189bf612e0b63f08c
*/

#include <stdlib.h>
#include "op.h"
#include "corewar/corlist.h"
#include "io.h"

/*
**	GLOBAL LOOP VARIABLES
*/
int		g__loaded_cor = 0;
int		g__current_cycle_die = CYCLE_TO_DIE;
long double	g__cycle_count = 0;
char		*g__pool = NULL;
t_cor		*g__corlist = NULL;

void	live(t_cor *cor)
{
  static int current_nbr_live = 0;

  my_printf("Le joueur %d (%s) est en vie\n", cor->id_cor, cor->name);
  cor->live = 1;
  current_nbr_live++;
  if (current_nbr_live == NBR_LIVE)
    {
      cor = g__corlist;
      while (cor != NULL && cor->live == 1)
	{
	  cor->live = 0;
	  cor = cor->next;
	}
      if (cor == NULL && g__current_cycle_die > 1)
	{
	  g__current_cycle_die -= CYCLE_DELTA;
	}
      current_nbr_live = 0;
    }
}

int	delete_elem(t_cor *cor, int nb_cor)
{
  int	cor_dies;
  t_cor	*tmp;

  cor_dies = 0;
  tmp = cor->next;
  if (cor->live != 1 && nb_cor == 0)
    {
      cor_dies++;
      my_printf("Le joueur %d (%s) est mort\n", cor->id_cor, cor->name);
      g__corlist = tmp;
      free(cor);
    }
  else if (tmp != NULL && tmp->live != 1)
    {
      cor_dies++;
      my_printf("Le joueur %d (%s) est mort\n", tmp->id_cor, tmp->name);
      cor->next = tmp->next;
      free(tmp);
    }
  return (cor_dies);
}

/*
** checks if a cor is alive or not at CYCLE_TO_DIE
*/
int	is_cor_alive(t_cor *cor)
{
  int	nb_cor;
  t_cor	*buffer;

  nb_cor = 0;
  while (cor && nb_cor < g__loaded_cor)
    {
      buffer = cor->next;
      g__loaded_cor -= delete_elem(cor, nb_cor);
      cor = buffer;
      nb_cor++;
    }
  return (0);
}

/*
** Schedueler for .cor(s)
*/
void	cor_loop(t_cor *cor)
{
  int	current_cycle;

  current_cycle = 0;
  while (g__loaded_cor != 1)
    {
      while (cor)
	{
	  if (cor->pc[0] == 0x01)
	    {
	      live(cor);
	    }
	  if (cor->pc >= g__pool + MEM_SIZE -1)
	    cor->pc -= MEM_SIZE +1;
	  cor->pc += op_mgr(cor->pc);
	  cor = cor->next;
	}
      cor = g__corlist;
      current_cycle++;
      if (current_cycle >= g__current_cycle_die)
	current_cycle = is_cor_alive(g__corlist);
      g__cycle_count++;
    }
  cor = g__corlist;
  my_printf("Le joueur %d (%s) a gagné !!\n", cor->id_cor, cor->name);
}
