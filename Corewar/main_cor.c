/*
** main_cor.c for Corewar_2014_cor in /home/nguye_g/rendu/Corewar_2014/corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Thu Feb 20 12:18:12 2014 Mano S. NGUYEN
** Last update Sat Apr 12 20:47:47 2014 Mano S. NGUYEN
*/

#include <stdlib.h>
#include "io.h"
#include "corewar/load.h"

/*
** cf. loop.c
*/
extern t_cor	*g__corlist;
extern char	*g__pool;

void	end_cleaning()
{
  free(g__pool);
  free(g__corlist);
}

int	main(int argc, char *argv[])
{
  int	*cor_fd;

  /*
  ** NEED FULL ARGUMENT PARSER
  */
  if ((cor_fd = open_cor(1, argc, argv)) == NULL
      || (g__corlist = load_cor(cor_fd)) == NULL
      || pool_init()
      || pool_fill(cor_fd, g__corlist))
    {
      my_eputstr("Exiting...\n");
      return (1);
    }
  my_putstr("\nRunning main loop...\n");
  cor_loop(g__corlist);
  end_cleaning();
  return (0);
}
