/*
** corlist.c for Corewar_2014 in /home/nguye_g/rendu/Corewar_2014/Corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Tue Apr  8 12:09:29 2014 Mano S. NGUYEN
** Last update Sat Apr 12 17:21:53 2014 Mano S. NGUYEN
*/

#include <stdlib.h>
#include "op.h"
#include "io.h"
#include "c_header.h"
#include "memory.h"
#include "strfuncs.h"
#include "corewar/corlist.h"

/*
** cf. loop.c
*/
extern int	g__loaded_cor;
extern int	g__current_cycle_die;

/*
** t_cor	*corlist_new(int fd)
**
** Create a new initialised cor chained list element
** and load header data from file descriptor into it
**
*/
t_cor	*corlist_new(int fd)
{
  t_cor	*out;

  if ((out = malloc(sizeof(t_cor))) == NULL)
    {
      my_eputstr("Cannot malloc cor chained list new element\n");
      return (NULL);
    }
  out->id_cor = g__loaded_cor++;
  out->live = 0;
  out->count_before_death = g__current_cycle_die;
  out->cycle_to_exec = 0;
  my_memclear(out->reg, REG_NUMBER * REG_SIZE);
  (out->reg)[0][0] = (char)(out->id_cor);
  out->carry = 0;
  out->pc = NULL;
  out->next = NULL;
  my_printf("Loading .cor header [ID: %d]... ", out->id_cor);
  if (corlist_load(out, fd))
    {
      free(out);
      return (NULL);
    }
  return (out);
}

/*
** int	corlist_load(t_cor *in, int fd)
**
** Load header from file descriptor in t_cor struct
**
*/
int		corlist_load(t_cor *in, int fd)
{
  header_t	*tmp;

  if ((tmp = c_header_read(fd)) == NULL)
    {
      my_printf("\nCannot read .cor file ID: %d\n", in->id_cor);
      return (1);
    }
  if ((my_getendian() == MEM_BIG_ENDIAN && tmp->magic != COREWAR_EXEC_MAGIC) ||
      ((my_getendian() == MEM_LITTLE_ENDIAN
	&& tmp->magic != my_memrev(COREWAR_EXEC_MAGIC))))
    {
      my_printf("\nFile is not a corewar executable\n", in->id_cor);
      return (2);
    }
  my_strcpy(in->name, tmp->prog_name);
  my_strcpy(in->comment, tmp->comment);
  if (my_getendian() == MEM_BIG_ENDIAN)
    in->size = tmp->prog_size;
  else
    in->size = my_memrev(tmp->prog_size);
  free(tmp);
  my_printf("[DONE]\n *Champion's name: %s\n *Comment: %s\n *Size: %d\n",
	    in->name, in->comment, in->size);
  return (0);
}

/*
** t_cor	*corlist_add(t_cor **list, t_cor *new)
**
** Add a new element at the end of a cor chained list
**
*/
t_cor	*corlist_add(t_cor **list, t_cor *new)
{
  t_cor	*buffer;

  if (new == NULL)
    return (NULL);
  if (*list == NULL)
    *list = new;
  else
    {
      buffer = *list;
      while (buffer->next != NULL)
	buffer = buffer->next;
      buffer->next = new;
    }
  return (*list);
}
