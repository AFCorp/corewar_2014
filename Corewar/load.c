/*
** load.c for Corewar_2014* in /home/nguye_g/rendu/Corewar_2014/Corewar
** 
** Made by Mano S. NGUYEN
** Login   <nguye_g@epitech.net>
** 
** Started on  Wed Apr  9 16:39:19 2014 Mano S. NGUYEN
** Last update Sat Apr 12 18:16:11 2014 nato_i
*/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "io.h"
#include "op.h"
#include "memory.h"
#include "corewar/corlist.h"

/*
** cf. loop.c
*/
extern int	g__loaded_cor;
extern char *	g__pool;

/*
** int	*open_cor(int start, int ac, char *av[])
**
** Open each .cor file passed in command line from start index to end of argv
** Return a file descriptor array
**
*/
int	*open_cor(int start, int ac, char *av[])
{
  int	i;
  int	*fd;

  if ((fd = malloc(((ac - start) + 1) * sizeof(int))) == NULL)
    {
      my_eputstr("Cannot malloc FD array\n");
      return (NULL);
    }
  i = 0;
  while (av[start] != NULL)
    {
      my_printf("Opening file: %s...\n", av[start]);
      if ((fd[i] = open(av[start], O_RDONLY)) != -1)
	++i;
      else
	my_printf("Cannot open file '%s'\n", av[start]);
      ++start;
    }
  fd[i] = -1;
  if (fd[0] == -1)
    {
      free(fd);
      return (NULL);
    }
  return (fd);
}

/*
** t_cor	*load_cor(int fd[])
**
** Init cor chained list and load header data for each file descriptor
** Return a pointer to cor chained list
**
*/
t_cor	*load_cor(int fd[])
{
  int	i;
  t_cor	*cor_list;

  i = 0;
  cor_list = NULL;
  my_putchar('\n');
  while (fd[i] != -1)
    {
      if (corlist_add(&cor_list, corlist_new(fd[i])) == NULL)
	return (NULL);
      ++i;
    }
  return (cor_list);
}


/*
** int	load_code(int fd, char *addr)
**
** Load .cor file code into
** pool at address *addr
**
*/
void	load_code(int fd, char *addr, t_cor *corinfo)
{
  int	rb;
  char	buffer[128];

  my_printf("Loading champion [ID: %d] '%s' into pool at address %p\n",
	    corinfo->id_cor, corinfo->name, addr);
  while ((rb = read(fd, buffer, 128)) > 0)
    {
      my_memcpy(addr, buffer, rb);
      corinfo->pc = addr;
      addr += rb;
    }
}

/*
** int	pool_alloc()
**
** Malloc a memory pool of MEM_SIZE bytes
** and store the address in g__pool global
**
*/
int	pool_init()
{
  my_putstr("\nAllocating and initializing pool... ");
  if ((g__pool = malloc(MEM_SIZE)) == NULL)
    {
      my_eputstr("[FAIL]\nCannot malloc pool\n");
      return (1);
    }
  my_printf("[DONE]\nPool address: %p\n\n", g__pool);
  my_memclear(g__pool, MEM_SIZE);
  return (0);
}

/*
** int	pool_fill(int fd[])
**
** Read code from all .cor file
** and store it into pool
**
*/
int	pool_fill(int fd[], t_cor *list)
{
  int	i;
  int	pool_level;
  t_cor	*corinfo;

  i = 0;
  pool_level = MEM_SIZE / g__loaded_cor;
  corinfo = list;
  while (fd[i] > -1 && corinfo != NULL)
    {
      load_code(fd[i], (void *)(g__pool + (i * pool_level)), corinfo);
      corinfo = corinfo->next;
      ++i;
    }
  i = 0;
  my_printf("Champion loaded: %d\nPool Level: %d\n\n", g__loaded_cor, pool_level);
  while (fd[i] > -1)
    {
      my_printf("Closing file descriptor: %d...\n", fd[i]);
      close(fd[i]);
      ++i;
    }
  my_putstr("Freeing file descriptor array...\n");
  free(fd);
  return (0);
}
