/*
** nb_oct.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Fri Apr  4 16:23:21 2014 potier_g
** Last update Tue Apr  8 23:27:03 2014 Guillaume
*/

#include <stdlib.h>
#include "op.h"
#include "asm/asm.h"

int	detect_fonct_oct(char *oct)
{
  int	sign;

  if ((sign = check_error(oct, 0) + 1) == -2)
    return (-1);
  if (sign == 1 || sign == 9 || sign == 12 || sign == 15)
    return (1);
  return (2);
}

int	detect_oct2(char *oct, char *oct0)
{
  int	sign;

  if (oct == NULL)
    return (0);
  if (oct[0] == '%')
    {
      if (oct[1] == ':')
	return (2);
      else if (oct[1] >= '0' && oct[1] <= '9')
	{
	  if ((sign = check_error(oct0, 0)) == -2)
	    return (-1);
	  if (sign == 9 || sign == 10 || sign == 11 || sign == 12
	      || sign == 15)
	    return (2);
	  else
	    return (4);
	}
    }
  if (oct[0] == 'r' && (oct[1] >= '0' && oct[1] <= '9'))
    return (1);
  if (oct[0] >= '0' && oct[0] <= '9')
    return (IND_SIZE);
  return (detect_fonct_oct(oct));
}

int	nb_oct_to_line(char *line)
{
  int	nb_oct;
  char	*oct;
  char	*oct0;
  int	nb;

  nb = 0;
  nb_oct = 0;
  oct0 = my_detect_str(line, 0);
  while (nb <= 3)
    {
      if ((oct = my_detect_str(line, nb)) == NULL)
	return (nb_oct);
      nb_oct = nb_oct + detect_oct2(oct, oct0);
      nb++;
    }
  return (nb_oct);
}

/* int	init_nb_oct(t_asm *p, int line) */
/* { */
/*   int	pos; */
/*   int	nb_oct; */
/*   char	*my_line; */
/*   char	*oct0; */
/*   int	detect; */

/*   pos = p->pos_word; */
/*   nb_oct = 0; */
/*   my_line = my_recup_line(p->file, p->pos_line); */
/*   oct0 = my_detect_str(my_line, 0); */
/*   if (line < p->pos_line) */
/*     { */
/*       while (pos > 0) */
/* 	{ */
/* 	  if ((detect = detect_oct2(my_detect_str(my_line, --pos), oct0)) == -1) */
/* 	    return (-1); */
/* 	  nb_oct = nb_oct + detect; */
/* 	} */
/*       return (nb_oct); */
/*     } */
/*   if (line > p->pos_line) */
/*     { */
/*       while (pos < 3) */
/* 	{ */
/* 	  if ((detect = detect_oct2(my_detect_str(my_line, ++pos), oct0)) == -1) */
/*             return (-1); */
/* 	  nb_oct = nb_oct + detect; */
/* 	} */
/*       return (nb_oct); */
/*     } */
/*   return (0); */
/* } */

int	detect_size(int line, t_asm *p)
{
  char	*my_line;
  int	nb_oct;
  int	detect;

  nb_oct = 0;
  if (line < p->pos_line)
    {
      while (line < p->pos_line - 1)
	{
	  line++;
	  if ((my_line = my_recup_line(p->file, line)) == NULL)
	    return (-1);
	  detect = nb_oct_to_line(my_line);
	  nb_oct = nb_oct + detect;
	}
      return ((nb_oct) * -1);
    }
  if (line > p->pos_line)
    {
      while (line > p->pos_line)
        {
	  line--;
	  if ((my_line = my_recup_line(p->file, line)) == NULL)
	    return (-1);
	  detect = nb_oct_to_line(my_line);
	  nb_oct = nb_oct + detect;
        }
      return (nb_oct);
    }
  return (0);
}
