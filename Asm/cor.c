/*
** cor.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Mon Apr  7 00:07:28 2014 potier_g
** Last update Tue Apr  8 22:40:26 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"
#include "strfuncs.h"

char	*cor(char *name)
{
  char	*new;
  int	a;

  if ((new = my_alloc(my_strlen(name))) == NULL)
    return (NULL);
  a = 0;
  while (name[a] != '.')
    {
      new[a] = name[a];
      a++;
    }
  new[a] = '\0';
  if ((new = my_strcat(new, ".cor")) == NULL)
    return (NULL);
  return (new);
}
