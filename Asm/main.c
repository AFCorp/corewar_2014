/*
** main.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Wed Mar 26 22:17:04 2014 potier_g
** Last update Tue Apr  8 22:46:14 2014 Guillaume
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "asm/asm.h"
#include "c_header.h"
#include "op.h"
#include "io.h"

/* prend le nom du fichier et retourne 1 si extention = ".s" */

int	check_extention(char *name)
{
  int	a;

  a = 0;
  while (name[a] != '.' && name[a] != '\0')
    a++;
  if (my_match(name + a, ".s") == -1)
    return (-1);
  return (1);
}

int	my_init_asm(int ac, char **av, t_asm *p)
{
  if (ac < 2)
    return (write(2, "please put an argument...\n", 26));
  else if (check_extention(av[1]) == -1)
    return (write(2, "usage : ./asm file.s\n", 21));
  else if ((p->file = my_read(av[1], p)) == NULL)
    return (-1);
  else if (my_check_header(p) == -1)
    return (-1);
  return (0);
}

int	my_asm(t_asm *p)
{
  int	a;
  char	*line;

  p->size = 0;
  a = 0;
  if ((p->cor = my_alloc(1)) == NULL)
    return (-1);
  p->cor[0] = 0;
  while ((line = my_recup_line(p->file, a)) != NULL)
    {
      if (my_analyse(line, a, p) == -1)
	return (-1);
      a++;
    }
  return (0);
}

int			main(int ac, char **av)
{
  t_asm			p;
  struct header_s	*h;
  char			*new;
  int			size;

  p.size = 0;
  if (my_init_asm(ac, av, &p) != 0)
    return (-1);
  if ((new = cor(av[1])) == NULL)
    return (-1);
  if ((p.cor_fd = open(new, O_WRONLY | O_TRUNC | O_CREAT,
		       S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1)
    return (-1);
  if ((size = my_size_prog(p.file)) == -1)
    return (-1);
  h = c_header_create(p.name, p.comment, size);
  c_header_write(p.cor_fd, h);
  if (my_asm(&p) != 0)
    return (-1);
  my_printf("name => %s\n", p.name);
  my_printf("comment => %s\n", p.comment);
  return (0);
}
