/*
** my_fonct01.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Mon Mar 31 15:44:16 2014 potier_g
** Last update Sat Apr 12 22:03:07 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"
#include "io.h"

int	my_pow_rec(int nb, int power)
{
  int	result;

  if (power == 0)
    return (1);
  if (power == 1)
    result = nb;
  else
    result = nb * my_power_rec(nb, power - 1);
  return (result);
}

char	*my_strdup(char *str)
{
  char	*new;
  int	a;

  a = 0;
  if (str == NULL)
    return (NULL);
  if ((new = my_alloc(my_strlen(str))) == NULL)
    return (NULL);
  while (str[a] != '\0')
    {
      new[a] = str[a];
      a++;
    }  
  new[a] = 0;
  return (new);
}

/* Prend la taille alloué et retourne l'adresse */

char	*my_alloc(int size)
{
  char	*str;

  if (size < 0)
    size = 1;
  if ((str = malloc(size)) == NULL)
    {
      my_putstr("\033[1;31mCan't perform malloc\n \033[0;0m");
      return (NULL);
    }
  return (str);
}

/* Prend deux strings et retourne leur concatenation */

char	*my_strcat(char *str1, char *str2)
{
  int	a;
  int	b;
  char	*ret;

  if (str1 == NULL || str2 == NULL)
    return (NULL);
  if ((ret = my_alloc(my_strlen(str1) + my_strlen(str2) + 1)) == NULL)
    return (NULL);
  a = 0;
  b = 0;
  while (str1[a] != '\0')
    ret[b++] = str1[a++];
  a = 0;
  while (str2[a] != '\0')
    ret[b++] = str2[a++];
  ret[b] = '\0';
  return (ret);
}
