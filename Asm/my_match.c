/*
** my_match.c for Corewar_2014_cor in /home/potierg/corewar_2014/Asm
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Fri Mar 21 15:36:54 2014 potier_g
** Last update Tue Apr  8 21:31:11 2014 Guillaume
*/

#include <stdlib.h>
#include "io.h"
#include "op.h"

/* Va permetre de comparer 2 strings retourne  si identique ou -1 */

int	my_match(char *str1, char *str2)
{
  int	a;

  a = 0;
  if (str1 == NULL || str2 == NULL)
    return (-1);
  if (my_strlen(str1) != my_strlen(str2))
    return (-1);
  while (str1[a] != '\0' && str2[a] != '\0')
    {
      if (str1[a] != str2[a])
	return (-1);
      a++;
    }
  return (1);
}
