/*
** my_analyse.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Sat Mar 29 15:55:17 2014 potier_g
** Last update Thu Apr 10 03:11:19 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"
#include "op.h"
#include "io.h"
#include "strfuncs.h"

/*  prend un mot ainsi que le numero de la ligne et retourne un negatif si erreur */

int	check_error(char *word, int line)
{
  int	a;
  int	len;

  a = 0;
  if (word == NULL || word[0] == 0)
    return (-1);
  while (a < 16)
    {
      if (my_match(op_tab[a].mnemonique, word) == 1)
	return (a);
      a++;
    }
  if (my_match(word, ".name") == 1 || my_match(word, ".comment") == 1)
    return (-1);
  len = my_strlen(word) - 1;
  if (word[len] == ':')
    return (-1);
  my_printf("\033[1;31mSyntax Error Line -> %d\n \033[0;0m", line);
  return (-2);
}

int	analyse_fonct(char *line, int sign, int l, t_asm *p)
{
  char  *oct1;
  char	*oct2;
  char	*oct3;
  
  if (sign < 1)
    return (1);
  my_convert_ascii(sign, p->cor_fd);
  oct1 = my_detect_str(line, 1);
  oct2 = my_detect_str(line, 2);
  oct3 = my_detect_str(line, 3);
  p->oct0 = my_detect_str(line, 0);
  if (sign != 1 && sign != 9  && sign != 12 && sign != 15)
    if (reconize_oct(oct1, oct2, oct3, p) == -1)
      {
  	my_printf("\033[1;31mSyntax Error Line -> %d\n \033[0;0m", l);
  	return (-1);
      }
  if (asign_oct(oct1, p, l, 1) == -1 || asign_oct(oct2, p, l, 2) == -1
      || asign_oct(oct3, p, l, 3) == -1)
    {
      my_printf("\033[1;31mSyntax Error Line -> %d\n \033[0;0m", l);
      return (-1);
    }
  return (0);
}

int	my_analyse(char *line, int l, t_asm *p)
{
  int	sign;

  if (line == NULL)
    return (-1);
  if (line[0] == 0 || (line[0] == '\t' && line[1] == '\0'))
    return (0);
  if ((sign = check_error(my_detect_str(line, 0), l)) < -1)
    return (-1);
  if (analyse_fonct(line, sign + 1, l, p) == -1)
    return (-1);
  return (0);
}
