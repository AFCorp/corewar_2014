/*
** my_convert_dec.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Mon Mar 31 12:06:22 2014 potier_g
** Last update Tue Apr  8 22:29:42 2014 Guillaume
*/

#include <stdlib.h>
#include <unistd.h>
#include "io.h"
#include "asm/asm.h"

/* Prend un nombre ainsi que sa puissance x et retourne le nombre puisance x */

int	my_power_rec(int nb, int power)
{
  int	result;

  if (power == 0)
    return (1);
  if (power == 1)
    result = nb;
  else
    result = nb * my_power_rec(nb, power - 1);
  return (result);
}

/* Prend un int, la taille de la base et la base et retourne ce nombre convertit */

/* char	*my_convert_dec(int nbr, int len, char *base) */
/* { */
/*   int	cpt; */
/*   int	mem; */
/*   char	*rep; */
/*   int	cpt2; */

/*   cpt = 0; */
/*   if (nbr == 0) */
/*     return ("0\0"); */
/*   while (nbr >= my_power_rec(len, cpt)) */
/*     cpt++; */
/*   if ((rep = my_alloc(cpt + 1)) == NULL) */
/*     return (NULL); */
/*   cpt = cpt - 1; */
/*   cpt2 = 0; */
/*   while (cpt >= 0) */
/*     { */
/*       mem = (nbr / my_power_rec(len, cpt)); */
/*       nbr = nbr % my_power_rec(len, cpt); */
/*       rep[cpt2] = base[mem]; */
/*       cpt = cpt - 1; */
/*       cpt2 = cpt2 + 1; */
/*     } */
/*   rep[cpt2] = 0; */
/*   return (rep); */
/* } */

int	my_convert_ascii(int nbr, int fd)
{
  int	cpt;
  int	mem;
  char	*rep;
  int	cpt2;

  cpt = 0;
  if (nbr == 0)
    write(fd, "\0", 1);
  if (nbr < 0)
    return (my_convert_ascii(65535 + (nbr + 1), fd));
  while (nbr >= my_power_rec(256, cpt))
    cpt++;
  if ((rep = my_alloc(cpt + 1)) == NULL)
    return (-1);
  cpt = cpt - 1;
  cpt2 = 0;
  while (cpt >= 0)
    {
      mem = (nbr / my_power_rec(256, cpt));
      nbr = nbr % my_power_rec(256, cpt--);
      rep[cpt2++] = mem;
    }
  rep[cpt2] = 0;
  write(fd, rep, my_strlen(rep));
  return (1);
}

/* prend la base ainsi que le caractére et retourne la position de c dans la base */

int	my_recup_int_char(char *base, char c)
{
  int	cpt;

  cpt = 0;
  while (base[cpt] != c)
    {
      cpt = cpt + 1;
    }
  if (cpt == 0)
    return (0);
  return (cpt);
}

/* prend un nb, la longueur de la base ainsi que la base et retourne la conversion en décimale */

int	my_convert_in_to_dec(char *nbr, char *base)
{
  int	cpt;
  int	cpt2;
  int	mem;

  mem = 0;
  cpt = my_strlen(nbr) - 1;
  cpt2 = 0;
  while (cpt >= 0)
    {
      mem = mem + my_recup_int_char(base, nbr[cpt2])
	* my_power_rec(my_strlen(base), cpt);
      cpt = cpt - 1;
      cpt2 = cpt2 + 1;
    }
  return (mem);
}
