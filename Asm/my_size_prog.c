/*
** my_size_prog.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Mon Apr  7 00:56:10 2014 potier_g
** Last update Tue Apr  8 22:18:12 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"
#include "io.h"

int	my_size_prog(char *file)
{
  int	size;
  int	l;
  char	*line;
  int	sign;
  char	*word;

  size = 0;
  l = 0;
  while ((line = my_recup_line(file, l)) != NULL)
    {
      word = my_detect_str(line, 0);
      if ((sign = check_error(word, l)) == -2)
	return (-1);
      if (sign >= 0)
	{
	  size = size + nb_oct_to_line(line);
	}
      l++;
    }
  return (size);
}
