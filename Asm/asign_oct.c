/*
** asign_oct.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Wed Apr  2 13:59:57 2014 potier_g
** Last update Sat Apr 12 22:41:30 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"
#include "strfuncs.h"
#include "op.h"
#include "io.h"

int	detect_fonct(char *fonct, t_asm *p)
{
  int	nb_line;
  char	*line;
  int	end;
  int	nb_oct_fonct;

  nb_line = 0;
  end = 0;
  while ((line = my_recup_line(p->file, nb_line)) != NULL && end == 0)
    {
      if (my_match(my_detect_str(line, 0), my_strcat(fonct, ":")) == 1)
	end++;
      if (end == 0)
	nb_line++;
    }
  if ((nb_oct_fonct = detect_size(nb_line, p)) == -1)
    return (-1);
  oct_null(nb_oct_fonct, 2, p->cor_fd);
  return (0);
}

int	detect_int(char *nb)
{
  int	a;

  a = 0;
  if (nb == NULL || nb[0] == 0)
    return (-1);
  if (nb[0] == ':')
    return (0);
  while (nb[a] != '\0')
    {
      if (nb[a] < '0' || nb[a] > '9')
	return (0);
      a++;
    }
  return (1);
}

int	code_nbr_nb_oct(char *nb, int nb_oct, t_asm *p, int nb_l)
{
  int	nbr;

  if (nb == NULL)
    return (0);
  if (detect_int(nb) == 0)
    return (detect_fonct(nb + 1, p));
  nbr = my_get_nbr(nb);
  if (nb_oct == 1 && nbr > my_pow_rec(255, nb_oct))
    {
      my_printf("\033[1;31mno such register line %d\n\033[0;0m", nb_l);
      return (-1);
    }
  if (nb_oct == IND_SIZE && nbr > my_pow_rec(255, nb_oct))
    my_printf("\033[1;31mWarning Indirection to far line %d\n\033[0;0m", nb_l);
  if (nb_oct == DIR_SIZE && nbr > my_pow_rec(255, 3))
    my_printf("\033[1;31mWarning Diret too big line %d\n\033[0;0m", nb_l);
  return (oct_null(nbr, nb_oct, p->cor_fd));
}

int	code_oct(char *nb, t_asm *p, int nb_oct, int nb_l)
{
  if (code_nbr_nb_oct(nb, nb_oct, p, nb_l) == -1)
    return (-1);
  return (1);
}

int	asign_oct(char *oct, t_asm *p, int nb_l, int nb_o)
{
  int	sign;

  if ((sign = check_error(p->oct0, 0)) < -1)
    return (-1);
  p->pos_line = nb_l;
  p->pos_word = nb_o;
  if (oct == NULL || oct[0] == 0)
    return (1);
  if (oct[0] == '%' && (sign == 9 || sign == 10 || sign == 11 || sign == 12
			|| sign == 15))
    return (code_oct(oct + 1, p, IND_SIZE, nb_l));
  if (oct[0] == '%')
    return (code_oct(oct + 1, p, DIR_SIZE, nb_l));
  if (oct[0] == 'r')
    return (code_oct(oct + 1, p, 1, nb_l));
  if (oct[0] >= '0' && oct[0] <= '9')
    return (code_oct(oct, p, IND_SIZE, nb_l));
  return (-1);
}
