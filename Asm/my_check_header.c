/*
** my_check_header.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Wed Mar 26 23:31:38 2014 potier_g
** Last update Tue Apr  8 22:34:28 2014 Guillaume
*/

#include <stdlib.h>
#include <unistd.h>
#include "asm/asm.h"
#include "io.h"
#include "op.h"

/* prend la structure t_asm et header_s et retourne 1 si pas d'erreur */

int	my_check_name(t_asm *p)
{
  int	pos;
  char	*line;

  pos = 0;
  while ((line = my_recup_line(p->file, pos)) != NULL
  	 && my_match(my_detect_str(line, 0), ".name") == -1)
    pos++;
  if (line == NULL)
    {
      write(2, "ERROR : name doesn't exist\n", 27);
      return (-1);
    }
  line = my_detect_str(my_recup_line(p->file, pos), 1) + 1;
  if (my_strlen(line) > PROG_NAME_LENGTH)
    {
      line[PROG_NAME_LENGTH] = 0;
      write(2, "WARNING : name has too long\n", 28);
    }
  if ((p->name = my_strdup(line)) == NULL)
    return (-1);
  return (1);
}

/* prend la structure t_asm et header_s et retourne 1 si pas d'erreur */

int	my_check_comment(t_asm *p)
{
  int	pos;
  char	*line;

  pos = 0;
  while ((line = my_recup_line(p->file, pos)) != NULL
         && my_match(my_detect_str(line, 0), ".comment") == -1)
    pos++;
  if (line == NULL)
    {
      if ((p->comment = my_strdup("no comment!!!")) == NULL)
	return (-1);
      return (1);
    }
  line = my_detect_str(my_recup_line(p->file, pos), 1) + 1;
  if (line != NULL && my_strlen(line) > COMMENT_LENGTH)
    {
      line[COMMENT_LENGTH] = 0;
      write(2, "WARNING : comment has too long\n", 31);
    }
  if ((p->comment = my_strdup(line)) == NULL)
    return (-1);
  return (1);
}

int	my_check_size(t_asm *p)
{
  char	*line;
  int	l;

  p->size = 23;
  l = 0;
  while ((line = my_recup_line(p->file, l)) != NULL)
    l++;
  return (1);
}

/* prend la structure t_asm et header_s et retourne 1 si pas d'erreur sinon - 1*/

int	my_check_header(t_asm *p)
{
  if (my_check_name(p) == -1)
    return (-1);
  else if (my_check_comment(p) == -1)
    return (-1);
  if (my_check_size(p) == -1)
    return (-1);
  return (1);
}
