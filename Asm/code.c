/*
** code.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Mon Mar 31 12:30:02 2014 potier_g
** Last update Thu Apr 10 03:11:00 2014 Guillaume
*/

#include <stdlib.h>
#include "asm/asm.h"

/* prend le mot et retourne la valeur associé a son type */

char	*detect_oct(char *oct)
{
  if (oct == NULL)
    return ("00\0");
  else if (oct[0] == 'r')
    return ("01\0");
  else if (oct[0] == '%')
    return ("10\0");
  else if (oct[0] >= '0' && oct[0] <= '9')
    return ("11\0");
  return (NULL);
}

int	reconize_oct(char *oct1, char *oct2, char *oct3, t_asm *p)
{
  char	*bin;
  char	*oct;

  if ((bin = detect_oct(oct1)) == NULL)
    return (-1);
  if ((oct = detect_oct(oct2)) == NULL)
    return (-1);
  if ((bin = my_strcat(bin, oct)) == NULL)
    return (-1);
  if ((oct = detect_oct(oct3)) == NULL)
    return (-1);
  if ((bin = my_strcat(bin, oct)) == NULL)
    return (-1);
  if ((bin = my_strcat(bin, "00\0")) == NULL)
    return (-1);
  my_convert_ascii(my_convert_in_to_dec(bin, "01"), p->cor_fd);
  return (0);
}
