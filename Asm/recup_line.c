/*
** recup_line.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Thu Mar 27 13:02:49 2014 potier_g
** Last update Sun Apr 13 15:46:53 2014 tahar_w
*/

#include <stdlib.h>
#include "asm/asm.h"

/* prend le fichier .s ainsi que le numero de la ligne et retourne cette ligne */

char	*my_recup_line(char *file, int line)
{
  int	pos;
  int	a;
  int	b;
  int	len;
  char	*str;

  pos = 0;
  len = 0;
  a = 0;
  while (file[a] && pos < line)
    if (file[a++] == '\n')
      pos++;
  b = a;
  while (file[a] && file[a] != '\n')
    {
      len++;
      a++;
    }
  if ((str = my_alloc(len + 2)) == NULL || file[b] == '\0')
    return (NULL);
  a = 0;
  while (file[b] != '\0' && file[b] != '\n')
    str[a++] = file[b++];
  str[a] = '\0';
  return (str);
}

/* Prend un caractére et retourne 1 si ce caractére est valide sinon 0 */

int     char_select(char c)
{
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
      || (c >= '0' && c <= '9') || c == '.' || c == '%' || c == ':'
      || c == '\"' || c == '+' || c == '-')
    return (1);
  return (0);
}

/* Prend la ligne, la postion du mot, et la position initial , et retourne la nouvelle position*/

int	detect_pos(char *line, int nb_word, int pos)
{
  int	nb;

  nb = 0;
  while (line[pos] != '\n' && line[pos] != '\0')
    {
      if (char_select(line[pos]) == 1)
        {
          if (nb == nb_word)
            return (pos);
          nb++;
          while (char_select(line[pos]) == 1 && line[pos] != '\n')
            pos++;
	}
      if (line[pos] == '\"')
	{
	  nb++;
	  while (line[pos + 1] && line[pos + 1] != '\"')
	    pos++;
	}
      pos++;
    }
  return (pos);
}

/* Prend la ligne et la position du début d'un mot et retourne la longueur du mot */

int     detect_len(char *line, int pos)
{
  int   a;
  int   len;

  a = pos;
  len = 1;
  while (line[a + 1] != '\0' && line[a + 1] != '\n')
    {
      if (line[pos] == '\"')
        {
          a++;
          while (line[a] != '\"')
            {
              a++;
              len++;
            }
          return (len);
        }
      if (char_select(line[a + 1]) == 0)
        return (len);
      a++;
      len++;
    }
  return (len);
}

/* prend la ligne et la position du mot et retourne ce mot ou NULL si erreur*/

char	*my_detect_str(char *line, int nb_word)
{
  int	pos;
  int	len;
  int	a;
  char	*str;

  pos = 0;
  if (line == NULL)
    return (NULL);
  if (line[0] == 0 || (line[0] == '\t' && line[1] == '\0'))
    return (NULL);
  while (char_select(line[pos]) == 0 && line[pos] != '\n' && line[pos] != '\0')
    pos++;
  pos = detect_pos(line, nb_word, pos);
  if (line[pos] == '\0')
    return (NULL);
  len = detect_len(line, pos);
  if ((str = my_alloc(len + 1)) == NULL)
    return (NULL);
  a = 0;
  while (a < len)
    str[a++] = line[pos++];
  str[a] = 0;
  return (str);
}
