/*
** my_read.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Wed Mar 26 22:38:46 2014 potier_g
** Last update Tue Apr  8 20:11:39 2014 tahar_w
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "io.h"
#include "asm/asm.h"

/* Prend le nom d'un fichier ainsi que la structure t_asm et retourne dans un char * le contenue du fichier */

char	*my_read(char *filename, t_asm *p)
{
  char	*str;
  char	*tmp;
  int	len;
  int	buff;

  buff = 1024;
  if ((p->s_fd = open(filename, O_RDONLY)) == -1)
    {
      my_printf("\033[1;31mFile %s not accessible\n \033[0;0m", filename);
      return (NULL);
    }
  if ((str = my_alloc(1)) == NULL)
    return (NULL);
  else if ((tmp = my_alloc(buff + 1)) == NULL)
    return (NULL);
  str[0] = 0;
  while (42)
    {
      if ((len = read(p->s_fd, tmp, buff)) <= 0)
	return (str);
      tmp[len] = '\0';
      if ((str = my_strcat(str, tmp)) == NULL)
	return (NULL);
    }
  return (NULL);
}
