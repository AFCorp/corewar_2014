/*
** oct_null.c for asm in /home/potierg/rendu/corewar
** 
** Made by potier_g
** potier_g   <potierg@epitech.net>
** 
** Started on  Sat Apr  5 13:08:05 2014 potier_g
** Last update Thu Apr 10 03:10:48 2014 Guillaume
*/

#include <stdlib.h>
#include <unistd.h>
#include "asm/asm.h"
#include "op.h"

int	oct_null(int oct, int nb_oct, int fd)
{
  if (oct >= 0)
    {
      if (nb_oct >= 2 && oct < 256)
	{
	  write(fd, "\0", 1);
	}
      if (nb_oct == 4 && oct < 4096)
	{
	  write(fd, "\0", 1);
	}
      if (nb_oct == 4 && oct < 65536)
	{
	  write(fd, "\0", 1);
	}
    }
  my_convert_ascii(oct, fd);
  return (1);
}
